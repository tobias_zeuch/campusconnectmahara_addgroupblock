<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'about');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


// Get plugin version
require('./version.php');
$versioninfo = $config->release . ' (' . $config->version . ')';

// Get required extensions versions
$extensions = curl_version();

$smarty = smarty();
$smarty->assign('subtitle', get_string('aboutplugin', 'artefact.campusconnect'));
$smarty->assign('plugindesc', get_string('plugindesc', 'artefact.campusconnect', '<a href="http://freeit.de/en/ecsa/index.html" target="_blank">', '</a>'));
$smarty->assign('plugintitle', get_string('plugintitle', 'artefact.campusconnect'));
$smarty->assign('pluginversion', get_string('pluginversion', 'artefact.campusconnect', $versioninfo));
$smarty->assign('plugincopyright', get_string('plugincopyright', 'artefact.campusconnect', '<a href="http://www.synergy-learning.com/" target="_blank">', '</a>'));
$smarty->assign('extensions', get_string('extensions', 'artefact.campusconnect'));
$smarty->assign('curlversion', get_string('curlversion', 'artefact.campusconnect', $extensions['version']));
$smarty->assign('opensslversion', get_string('opensslversion', 'artefact.campusconnect', $extensions['ssl_version']));
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->display('artefact:campusconnect:about.tpl');
