<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
// ADMIN, MENUITEM & CAMPUSCONNECT_SUBPAGE defined below.

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
safe_require('artefact', 'campusconnect');

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


$firstgroupid = get_field_sql('SELECT id FROM {group} WHERE deleted=0 ORDER BY id LIMIT 1');
$groupid = param_variable('groupid', $firstgroupid);
$filteringenabled = get_config_plugin('artefact', 'campusconnect', 'filteringenabled');

if ($groupid && group_user_access($groupid, $USER->get('id')) == 'admin') {
    if ($USER->get('admin')) {
        define('ADMIN', 1);
        define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
        define('MENUITEM', 'configsite/campusconnect');
        define('CAMPUSCONNECT_SUBPAGE', 'filtering');
    }
    else {
        if ($filteringenabled) {
            define('PUBLIC', 1);
            $group = get_record('group', 'id', $groupid);
            define('TITLE', $group->name);
            define('MENUITEM', 'groups/filtering');
        } else {
            throw new AccessDeniedException("CampusConnect course link filtering not enabled for group admins.");
        }
    }
}
elseif ($USER->get('admin')) {
    define('ADMIN', 1);
    define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
    define('MENUITEM', 'configsite/campusconnect');
    define('CAMPUSCONNECT_SUBPAGE', 'filtering');
}
else {
    throw new AccessDeniedException("Cannot access CampusConnect course link filtering.");
}


$filterattributes = get_config_plugin('artefact', 'campusconnect', 'filterattributes');
if (isset($filterattributes)) {
    $attributes = explode(',', $filterattributes);
}
else {
    $attributes = array();
}
$count = count($attributes);
// Add drop-down box for new attribute
$attributes[] = null;
$count += 1;


// General settings form static elements
$generalelements = array(
    'groupid' => array(
        'type' => 'html',
        'value' => $groupid,
    ),
    // Add attribute count, needed for
    // form rendering and processing
    'attribcount' => array(
        'type' => 'html',
        'value' => $count,
    ),
    'filteringenabled' => array(
        'type' => 'select',
        'title' => get_string('filteringenabled', 'artefact.campusconnect'),
        'options' => array(
            0 => get_string('filteringenabledadmin', 'artefact.campusconnect'),
            1 => get_string('filteringenabledgroup', 'artefact.campusconnect'),
        ),
        'defaultvalue' => (isset($filteringenabled) ? $filteringenabled : 0),
    ),
    'submit' => array(
        'type' => 'submit',
        'value' => get_string('savesettings', 'artefact.campusconnect'),
    ),
);
// Get all the attributes as options for drop-down menu
$data = get_records_menu('artefact_campusconnect_attribute', 'active', 1, 'attrib');
$options['0'] = get_string('noneselected', 'artefact.campusconnect');
if ($data) {
    foreach ($data as $item) {
        $options[$item] = $item;
    }
}

// General settings form dynamic elements: drop-down boxes for selected
// attributes and another "empty" one - to add new attribute
for ($i = 0; $i < $count; $i++) {
    $generalelements['attribute_'.$i] = array(
        'type' => 'select',
        'title' => get_string('attribute', 'artefact.campusconnect'),
        'options' => $options,
        'defaultvalue' => (isset($attributes[$i]) ? $attributes[$i] : '0'),
    );
}

// General filter settings form
$generalform = pieform(array(
    'name' => 'campusconnect_general_filter',
    'plugintype' => 'artefact',
    'pluginname' => 'campusconnect',
    'template' => 'filtergeneralform.php',
    'elements' => $generalelements,
));


// Course filtering form static elements
$filterelements = array(
    'groupid' => array(
        'type' => 'html',
        'value' => $groupid,
    ),
    // Add attribute count, needed for
    // form rendering and processing
    'attribcount' => array(
        'type' => 'html',
        'value' => $count,
    ),
    'submit' => array(
        'type' => 'submit',
        'value' => get_string('savechanges', 'artefact.campusconnect'),
    ),
);
// Course filtering form dynamic elements: drop-down boxes for selected
// attributes and another "empty" one - to add new attribute
$settings = unserialize(get_config_plugin('artefact', 'campusconnect', 'filtersettings_'.$groupid));
for ($i = 0; $i < $count-1; $i++) {
    $prefix = $attributes[$i];
    $filterelements[$prefix.'_useattrib'] = array(
        'type' => 'select',
        'title' => get_string('useattrib', 'artefact.campusconnect'),
        'defaultvalue' => (isset($settings[$prefix.'_useattrib']) ? $settings[$prefix.'_useattrib'] : 0),
        'options' => array(
            0 => get_string('no'),
            1 => get_string('yes'),
        ),
    );
    /*
    $filterelements[$prefix.'_allwords'] = array(
        'type' => 'select',
        'title' => get_string('allwords', 'artefact.campusconnect'),
        'defaultvalue' => (isset($settings[$prefix.'_allwords']) ? $settings[$prefix.'_allwords'] : 0),
        'options' => array(
            0 => get_string('no'),
            1 => get_string('yes'),
        ),
    );
    */
    $filterelements[$prefix.'_filterwords'] = array(
        'type' => 'text',
        'title' => get_string('filterwords', 'artefact.campusconnect'),
        'defaultvalue' => (isset($settings[$prefix.'_filterwords']) ? $settings[$prefix.'_filterwords'] : ''),
    );
}

// Course filtering form (admin)
$groupallform = pieform(array(
    'name' => 'campusconnect_groupall_filter',
    'plugintype' => 'artefact',
    'pluginname' => 'campusconnect',
    'template' => 'filtergroupallform.php',
    'elements' => $filterelements,
));


$filtergroupone = array();

// Course filtering form (group admin)
$grouponeform = pieform(array(
    'name' => 'campusconnect_groupone_filter',
    'plugintype' => 'artefact',
    'pluginname' => 'campusconnect',
    'template' => 'filtergrouponeform.php',
    'elements' => $filterelements,
));


// Set which form should be used
if ($USER->get('admin')) {
    $settingsform = $generalform;
    $filteringform = $groupallform;
} else {
    $settingsform = null;
    $filteringform = $grouponeform;
}

$smarty = smarty();
$smarty->assign('settingsform', $settingsform);
$smarty->assign('filteringform', $filteringform);
$smarty->assign('PAGEHEADING', TITLE);
if ($USER->get('admin')) {
    $smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
}
$smarty->display('artefact:campusconnect:filtering.tpl');


function campusconnect_general_filter_submit(Pieform $form, $values) {
    global $SESSION;
    $count = intval($values['attribcount']);
    $groupid = intval($values['groupid']);
    $attributes = array();
    for ($i = 0; $i < $count; $i++) {
        if ($values['attribute_'.$i] != '0') {
            $attributes[] = $values['attribute_'.$i];
        }
    }

    set_config_plugin('artefact', 'campusconnect', 'filterattributes', implode(',', $attributes));
    set_config_plugin('artefact', 'campusconnect', 'filteringenabled', $values['filteringenabled']);

    $SESSION->add_ok_msg(get_string('generalsettingssaved', 'artefact.campusconnect'));
    if (isset($values['groupid']) && is_numeric($values['groupid'])) {
        $redirect = get_config('wwwroot').'artefact/campusconnect/filtering.php?groupid='.$groupid;
    } else {
        $redirect = get_config('wwwroot').'artefact/campusconnect/filtering.php';
    }
    redirect($redirect);
}

function campusconnect_groupall_filter_submit(Pieform $form, $values) {
    global $SESSION;
    $groupid = intval($values['groupid']);
    // Unset unused values
    unset($values['groupid']);
    unset($values['attribcount']);
    unset($values['submit']);
    unset($values['sesskey']);

    set_config_plugin('artefact', 'campusconnect', 'filtersettings_'.$groupid, serialize($values));
    $SESSION->add_ok_msg(get_string('groupsettingssaved', 'artefact.campusconnect'));
    redirect(get_config('wwwroot').'artefact/campusconnect/filtering.php?groupid='.$groupid);
}

// See the above 'campusconnect_groupall_filter_submit' function!
function campusconnect_groupone_filter_submit(Pieform $form, $values) {
    global $SESSION;
    $groupid = intval($values['groupid']);
    // Unset unused values
    unset($values['groupid']);
    unset($values['attribcount']);
    unset($values['submit']);
    unset($values['sesskey']);

    set_config_plugin('artefact', 'campusconnect', 'filtersettings_'.$groupid, serialize($values));
    $SESSION->add_ok_msg(get_string('groupsettingssaved', 'artefact.campusconnect'));
    redirect(get_config('wwwroot').'group/view.php?id='.$groupid);
}
