<?php
/**
 * Pieforms: Advanced web forms made easy
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    pieforms
 * @subpackage element
 * @author     Nigel McNie <nigel@catalyst.net.nz>
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

/**
 * Provides a time picker for cron operations, in the form of
 * two dropdowns (for minutes and seconds).
 *
 * @param Pieform  $form  The form to render the element for
 * @param array $element  The element to render
 * @return string         The HTML for the element
 */
function pieform_element_crontime(Pieform $form, $element) {/*{{{*/
    $result = '';
    $name = Pieform::hsc($element['name']);
    $value = Pieform::hsc($form->get_value($element));
    if (empty($value) && isset($element['defaultvalue'])) {
        $value = $element['defaultvalue'];
    }
    else {
        $value = 300; // 300 seconds = 5 minutes 0 seconds
    }
    if (!empty($element['rules']['required']) && !isset($element['defaultvalue'])) {
        $value = 300; // 300 seconds = 5 minutes 0 seconds
    }
    $cron[0] = floor($value / 60); // Get minutes
    $cron[1] = $value % 60; // Get remaining seconds

    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;

    // Minute
    $minute = '<select name="' . $name . '_minute" id="' . $name . '_minute"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    for ($i = 0; $i <= 59; $i++) {
        $minute .= "\t<option value=\"$i\"" . (($cron[0] == $i) ? ' selected="selected"' : '') . ">" . sprintf("%02d", $i) . "</option>\n";
    }
    $minute .= "</select>\n";

    // Second
    $second = '<select name="' . $name . '_second" id="' . $name . '_second"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    for ($i = 0; $i <= 59; $i++) {
        $second .= "\t<option value=\"$i\"" . (($cron[1] == $i) ? ' selected="selected"' : '') . ">". sprintf("%02d", $i) . "</option>\n";
    }
    $second .= "</select>\n";

    $result = $minute . get_string('minutes', 'artefact.campusconnect') . '&nbsp;&nbsp;'
            . $second . get_string('seconds', 'artefact.campusconnect');
    return $result;
}/*}}}*/

/**
 * Gets the value of the crontime element from the request and converts
 * it into an integer of seconds.
 *
 * @param Pieform $form     The form the element is attached to
 * @param array   $element  The element to get the value for
 */
function pieform_element_crontime_get_value(Pieform $form, $element) {/*{{{*/
    $name = $element['name'];
    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;
    if ($form->is_submitted() && isset($global[$name . '_minute']) && isset($global[$name . '_second'])) {
        $time = $global[$name . '_minute'] * 60 + $global[$name . '_second'];
        if (false === $time) {
            return null;
        }
        return $time;
    }

    return null;
}/*}}}*/
