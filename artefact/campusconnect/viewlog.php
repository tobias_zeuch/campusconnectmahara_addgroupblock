<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
safe_require('artefact', 'campusconnect');

if (!$USER->get('admin')) {
    die('Admin only');
}

$clearlog = param_integer('clearlog', 0);
$confirmclearlog = param_integer('confirmclearlog', 0);
$sesskey = param_variable('sesskey', null);

$path = get_config('wwwroot').'artefact/campusconnect/';

if ($confirmclearlog && $USER && $USER->is_logged_in() &&
    $USER->get('sesskey') == $sesskey) {
    ArtefactTypeCampusconnectLog::clearlog();
    redirect($path.'index.php');
}
else if ($clearlog) {
    echo '<h2>Are you sure you want to clear all log entries?</h2>';
    echo '<a href="'.$path.'viewlog.php?confirmclearlog=1&sesskey='.$USER->get('sesskey').'">Yes</a>';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$path.'viewlog.php">No</a>';
    die();
}

echo '<a href="'.$path.'viewlog.php?clearlog=1">Clear log</a>';
echo '<pre>';
ArtefactTypeCampusconnectLog::outputlog();
echo '</pre>';
