<?php
/**
 * Pieforms: Advanced web forms made easy
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    pieforms
 * @subpackage element
 * @author     Nigel McNie <nigel@catalyst.net.nz>
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

/**
 * Provides a time picker, in the form of three dropdowns.
 *
 * @param Pieform  $form    The form to render the element for
 * @param array $element The element to render
 * @return string        The HTML for the element
 */
function pieform_element_time(Pieform $form, $element) {/*{{{*/
    $result = '';
    $name = Pieform::hsc($element['name']);
    $required = (!empty($element['rules']['required']));
    if ($required && !isset($element['defaultvalue'])) {
        $element['defaultvalue'] = time();
    }

    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;
    $dateisset = isset($element['defaultvalue']);
    $dateisset = $dateisset
                || ((isset($element['value']['hour']) || isset($global[$element['name'] . '_hour']))
                   && (isset($element['value']['minute']) || isset($global[$element['name'] . '_minute']))
                   && (isset($element['value']['second']) || isset($global[$element['name'] . '_second'])));
    // Hour
    $value = pieform_element_time_get_timeperiod_value('hour', 0, 23, $element, $form);
    $hour = '<select name="' . $name . '_hour" id="' . $name . '_hour"'
        . (!$required && !$dateisset ? ' disabled="disabled"' : '')
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    for ($i = 0; $i <= 23; $i++) {
        $hour .= "\t<option value=\"$i\"" . (($value == $i) ? ' selected="selected"' : '') . ">" . sprintf("%02d", $i) . "</option>\n";
    }
    $hour .= "</select>\n";

    // Minute
    $value = pieform_element_time_get_timeperiod_value('minute', 0, 59, $element, $form);
    $minute = '<select name="' . $name . '_minute" id="' . $name . '_minute"'
        . (!$required && !$dateisset ? ' disabled="disabled"' : '')
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    for ($i = 0; $i <= 59; $i++) {
        $minute .= "\t<option value=\"$i\"" . (($value == $i) ? ' selected="selected"' : '') . ">" . sprintf("%02d", $i) . "</option>\n";
    }
    $minute .= "</select>\n";

    // Second
    $value = pieform_element_time_get_timeperiod_value('second', 0, 59, $element, $form);
    $second = '<select name="' . $name . '_second" id="' . $name . '_second"'
        . (!$required && !$dateisset ? ' disabled="disabled"' : '')
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    for ($i = 0; $i <= 59; $i++) {
        $second .= "\t<option value=\"$i\"" . (($value == $i) ? ' selected="selected"' : '') . ">". sprintf("%02d", $i) . "</option>\n";
    }
    $second .= "</select>\n";


    // This pieform element is used for setting cron operations which run only on minutes and hours
    // (no seconds used), so to show seconds drop-down this must explicitly set to true!
    $showseconds = (isset($element['seconds']) ? $element['seconds'] : false);
    if ($showseconds) {
        $result = $hour . $minute . $second;
    }
    else {
        $result = $hour . $minute;
    }

    // Optional control
    if (!$required) {
        $optional = <<<EOF
        <script type="text/javascript">
            function {$name}_toggle(x) {
                var elements = [
                    $('{$name}_hour'),
                    $('{$name}_minute'),
                    $('{$name}_second')
                ];
                for (var i in elements) {
                    if (elements[i]) elements[i].disabled = x.checked;
                }
            }
        </script>
EOF;
        // @todo this needs cleaning up, namely:
        //   - get_string is a mahara-ism
        //   - 'optional' => true should be 'required' => false shouldn't it?
        $optional .= ' ' . $form->i18n('element', 'date', 'or', $element) . ' <input type="checkbox" '
            . (isset($element['defaultvalue']) ? '' : 'checked="checked" ')
            . 'name="' . $name . '_optional" id="' . $name . '_optional" onchange="' . $name . '_toggle(this)" '
            . 'tabindex="' . Pieform::hsc($element['tabindex']) . '">';
        $optional .= ' <label for="' . $name . '_optional">' . $form->i18n('element', 'date', 'notspecified', $element);

        $result .= $optional;
    }

    return $result;
}/*}}}*/

/**
 * Gets the value of the date element from the request and converts it into a
 * unix timestamp.
 *
 * @param Pieform $form    The form the element is attached to
 * @param array   $element The element to get the value for
 */
function pieform_element_time_get_value(Pieform $form, $element) {/*{{{*/
    $name = $element['name'];
    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;
    if ($form->is_submitted() && isset($global[$name . '_hour']) && isset($global[$name . '_minute']) && isset($global[$name . '_second'])) {
        $time = mktime($global[$name . '_hour'], $global[$name . '_minute'], $global[$name . '_second']);
        if (false === $time) {
            return null;
        }
        return $time;
    }

    return null;
}/*}}}*/


/** helper: used when rendering the element, to get the value for it */
function pieform_element_time_get_timeperiod_value($timeperiod, $min, $max, $element, Pieform $form) {/*{{{*/
    static $lookup = array(
        'hour' => 0,
        'minute' => 1,
        'second' => 2
    );
    $index = $lookup[$timeperiod];

    if (isset($element['value'][$index])) {
        $value = $element['value'][$index];
        if ($value < $min || $value > $max) {
            $value = $min;
        }
        return $value;
    }

    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;
    if (isset($global[$element['name'] . '_' . $timeperiod])) {
        $value = $global[$element['name'] . '_' . $timeperiod];
        if ($value < $min || $value > $max) {
            $value = $min;
        }
        return $value;
    }

    $value = time();

    if (isset($element['defaultvalue'])) {
        $value = $element['defaultvalue'];
    }

    switch ($timeperiod) {
        case 'hour':
            $value = date('G', $value);
            break;
        case 'minute':
            $value = date('i', $value);
            break;
        case 'second':
            $value = date('s', $value);
            break;
    }

    return $value;
}/*}}}*/
