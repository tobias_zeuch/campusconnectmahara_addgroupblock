<?php
/**
 *
 * @package    mahara
 * @subpackage campusconnect-courselinklist
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @author     Tobias Zeuch <tobias.zeuch@kit.edu>
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

class PluginBlocktypeCourselinklist extends PluginBlocktype {

    public static function get_title() {
        return get_string('title', 'blocktype.courselinklist/campusconnect');
    }

    /**
     * Optional method. If exists, allows this class to decide the title for 
     * all blockinstances of this type
     */
    public static function get_instance_title(BlockInstance $bi) {
        $configdata = $bi->get('configdata');

        if (!empty($configdata['artefactid'])) {
            return $bi->get_artefact_instance($configdata['artefactid'])->get('title');
        }
        return '';
    }

    /**
     * returns the description for this blocktype, defined in the language file
     * @return string
     */
    public static function get_description() {
        return get_string('description', 'blocktype.courselinklist/campusconnect');
    }

    /**
     * the category where the blocktype will appear when editing a page
     * @return string
     */
    public static function get_categories() {
        return array('general');
    }

    /**
     * renders the blockinstance
     * @param BlockInstance $instance
     * @param boolean $editing
     * @return string
     */
    public static function render_instance(BlockInstance $instance, $editing=false) {
        $groupid = $instance->get_view()->get('group');
        if (!$groupid) {
            return '';
        }
        global $USER;

        // Get all available course links from all ECS servers that are online
        $sql = "SELECT cl.* FROM
                {artefact_campusconnect_courselink} cl
                LEFT JOIN {artefact_campusconnect_ecs} ecs ON (ecs.artefact = cl.ecsid)
                WHERE ecs.enabled = 2";
        $links = get_records_sql_assoc($sql, array());

        if ($links) {
            foreach ($links as &$link) {
                $link->metadata = unserialize($link->metadata);
            }
        }

        // Get group filtering criteria
        $attributes = unserialize(get_config_plugin('artefact', 'campusconnect', 'filtersettings_'.$groupid));
        $criteria = get_config_plugin('artefact', 'campusconnect', 'filterattributes');
        $criteria = explode(',', $criteria);

        // If criteria exists, then filter course links
        // Filtering process: "unset" all course links
        // that do not match certain criterion...
        if ($attributes && $criteria && $links) {
            $filtered = array();
            foreach ($criteria as $criterion) {
                // Filter course links according to each $criterion,
                // if it exists and is enabled (== should be used).
                if (isset($attributes[$criterion.'_useattrib']) &&
                    $attributes[$criterion.'_useattrib'] == 1) {
                    // Filter course links according to current $criterion
                    $filterwords = preg_replace('/\W+/', ' ', $attributes[$criterion.'_filterwords']);
                    $filterwords = explode(",", $filterwords);

                    foreach ($links as $key => $value) {
                        $linkvalues = (isset($value->metadata->$criterion) ? $value->metadata->$criterion : '');
                        $linkvalues = preg_replace('/\W+/', ' ', $linkvalues);
                        $linkvalues = explode(" ", $linkvalues);

                        // If course link doesn't match any of the filter criteria then
                        // it should be filtered (that is removed/unset).
                        foreach ($linkvalues as $linkvalue) {
                            if (in_array($linkvalue, $filterwords)) {
                                // Filter criteria match, so add link to filtered.
                                $filtered[] = $links[$key];
                            }
                        }
                    }
                }
            }
        }

        // Format course links that matched the criteria
        if (!empty($filtered)) {
            $access = false;
            // Is the user also the site admin?
            $siteadmin = $USER->get('admin');
            if ($siteadmin) {
                $access = true;
            }
            // Is the user also the group admin? If that is the casem then
            // is the course link filtering config made at group admin level?
            $filteringenabled = get_config_plugin('artefact', 'campusconnect', 'filteringenabled');
            if ($groupid && group_user_access($groupid, $USER->get('id')) == 'admin' && $filteringenabled) {
                $access = true;
            }

            $data = array();
            safe_require('artefact', 'campusconnect');
            foreach ($filtered as $link) {
                $status = get_field('artefact_campusconnect_status', 'status', 'url', $link->url, 'userid', $USER->get('id'));
                $data[$link->id] = array(
                    'id'           => $link->id,
                    'title'        => $link->title,
                    'url'          => ArtefactTypeCourselink::check_redirect($link),
                    'organisation' => (isset($link->metadata->destinationForDisplay) ? $link->metadata->destinationForDisplay : ''),
                    'lang'         => (isset($link->metadata->lang) ? $link->metadata->lang : ''),
                    'term'         => (isset($link->metadata->term) ? $link->metadata->term : ''),
                    'credits'      => (isset($link->metadata->credits) ? $link->metadata->credits : ''),
                    'status'       => (isset($link->metadata->status) ? $link->metadata->status : ''),
                    'coursetype'   => (isset($link->metadata->courseType) ? $link->metadata->courseType : ''),
                    'enrolment'    => ($status ? get_string($status, 'artefact.campusconnect') : get_string('not_enroled', 'artefact.campusconnect')),
                );
            }
            $result = array(
                'links'     => $data,
                'access'    => $access,
                'groupid'   => $groupid,
            );
        }
        // If no course links matched criteria
        else {
            $result = array();
        }
        $smarty = smarty_core();
        $smarty->assign('data', $result);
        $smarty->assign('blockinstanceid', $instance->get('id'));
        return $smarty->fetch('artefact:campusconnect:courselinklist.tpl');
    }

    /**
     * there should not be more than one block of this type per page because it
     * would show the same elements anyway
     * @return boolean
     */
    public static function single_only() {
        return true;
    }

    /**
     * this blocktype is only applicable on the grouphomepage
     * @return /string
     */
    public static function get_viewtypes() {
        return array('grouphomepage');
    }
    
    /**
     * there's no config form attached to this block
     * @return boolean
     */
    public static function has_instance_config() {
        return (boolean)get_config_plugin('artefact', 'campusconnect', 'filteringenabled');
    }

    /**
     * because group pages shouldn't be copied anyway
     * @return string
     */
    public static function default_copy_type() {
        return 'nocopy';
    }

    public static function artefactchooser_element($default=null) {
        return array(
            'name'  => 'artefactid',
            'type'  => 'artefactchooser',
            'title' => get_string('blog', 'artefact.blog'),
            'defaultvalue' => $default,
            'blocktype' => 'courselinklist',
            'limit'     => 10,
            'selectone' => true,
            'artefacttypes' => array(''),
            'template'  => 'artefact:blog:artefactchooser-element.tpl',
        );
    }

    /**
     * Blog blocktype is only allowed in personal views, because currently
     * there's no such thing as group/site blogs
     */
//    public static function allowed_in_view(View $view) {
//        return $view->get('owner') != null;
//    }

}
