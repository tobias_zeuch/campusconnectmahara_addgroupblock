<?php
/**
 * Pieforms: Advanced web forms made easy
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    pieforms
 * @subpackage element
 * @author     Nigel McNie <nigel@catalyst.net.nz>
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

/**
 * Provides a cron picker, in the form of five dropdowns
 * (minute, hour, day of month, month, day of week).
 *
 *     * * * * *  command to execute 
 *     | | | | |
 *     | | | | --- day of week (0 - 6) (0 to 6 are Sunday to Saturday)
 *     | | | ----- month (1 - 12)
 *     | | ------- day of month (1 - 31)
 *     | --------- hour (0 - 23)
 *     ----------- min (0 - 59)
 *
 * @param Pieform $form   The form to render the element for
 * @param array $element  The element to render
 * @return string         The HTML for the element
 */
function pieform_element_cron(Pieform $form, $element) {/*{{{*/
    $result = '';
    $name = Pieform::hsc($element['name']);
    $value = Pieform::hsc($form->get_value($element));
    if (empty($value) && isset($element['defaultvalue'])) {
        $value = $element['defaultvalue'];
    }
    else {
        $value = '* * * * *';
    }
    if (!empty($element['rules']['required']) && !isset($element['defaultvalue'])) {
        $value = '* * * * *';
    }
    $cronvalues = explode(' ', $value);

    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;

    // Minute
    $minute = '<select name="' . $name . '_minute" id="' . $name . '_minute"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    $minute .= "\t<option value=\"*\"" . (($cronvalues[0] == '*') ? ' selected="selected"' : '') . ">*</option>\n";
    for ($i = 0; $i <= 59; $i++) {
        $minute .= "\t<option value=\"$i\"" . (($cronvalues[0] == (string)$i) ? ' selected="selected"' : '') . ">$i</option>\n";
    }
    $minute .= '</select>';

    // Hour
    $hour = '<select name="' . $name . '_hour" id="' . $name . '_hour"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    $hour .= "\t<option value=\"*\"" . (($cronvalues[1] == '*') ? ' selected="selected"' : '') . ">*</option>\n";
    for ($i = 0; $i <= 23; $i++) {
        $hour .= "\t<option value=\"$i\"" . (($cronvalues[1] == (string)$i) ? ' selected="selected"' : '') . ">$i</option>\n";
    }
    $hour .= '</select>';

    // Day
    $day = '<select name="' . $name . '_day" id="' . $name . '_day"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    $day .= "\t<option value=\"*\"" . (($cronvalues[2] == '*') ? ' selected="selected"' : '') . ">*</option>\n";
    for ($i = 1; $i <= 31; $i++) {
        $day .= "\t<option value=\"$i\"" . (($cronvalues[2] == (string)$i) ? ' selected="selected"' : '') . ">$i</option>\n";
    }
    $day .= '</select>';

    // Month
    $value = null; /*pieform_element_cron_get_timeperiod_value('month', 1, 12, $element, $form);*/
    $month = '<select name="' . $name . '_month" id="' . $name . '_month"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    $month .= "\t<option value=\"*\"" . (($cronvalues[3] == '*') ? ' selected="selected"' : '') . ">*</option>\n";
    for ($i = 1; $i <= 12; $i++) {
        $month .= "\t<option value=\"$i\"" . (($cronvalues[3] == (string)$i) ? ' selected="selected"' : '') . ">$i</option>\n";
    }
    $month .= "</select>\n";

    // Day of the week
    $weekday = '<select name="' . $name . '_weekday" id="' . $name . '_weekday"'
        . ' tabindex="' . Pieform::hsc($element['tabindex']) . "\">\n";
    $weekday .= "\t<option value=\"*\"" . (($cronvalues[4] == '*') ? ' selected="selected"' : '') . ">*</option>\n";
    for ($i = 0; $i <= 6; $i++) {
        $weekday .= "\t<option value=\"$i\"" . (($cronvalues[4] == (string)$i) ? ' selected="selected"' : '') . ">$i</option>\n";
    }
    $weekday .= "</select>\n";

    $result = $minute. $hour . $day . $month . $weekday;
    return $result;
}/*}}}*/

/**
 * Gets the value of the cron element from the request and converts it
 * into a cron string.
 *
 * @param Pieform $form    The form the element is attached to
 * @param array   $element The element to get the value for
 */
function pieform_element_cron_get_value(Pieform $form, $element) {/*{{{*/
    $name = $element['name'];
    $global = ($form->get_property('method') == 'get') ? $_GET : $_POST;
    if ($form->is_submitted() && isset($global[$name . '_minute']) && isset($global[$name . '_hour']) &&
        isset($global[$name . '_day']) && isset($global[$name . '_month']) && isset($global[$name . '_weekday'])) {
        $cron = implode(' ', array(
            $global[$name . '_minute'],
            $global[$name . '_hour'],
            $global[$name . '_day'],
            $global[$name . '_month'],
            $global[$name . '_weekday']
        ));
        return $cron;
    }

    return null;
}/*}}}*/
