<?php
echo $form_tag;
?>

<fieldset>
<legend><?php echo get_string('filteringsettings', 'artefact.campusconnect'); ?></legend>
<table class="fullwidth">
    <tbody>
        <tr>
            <td>
            <?php
                $attribconfig = get_config_plugin('artefact', 'campusconnect', 'filterattributes');
                $attributes = array();
                if (isset($attribconfig) && !empty($attribconfig)) {
                    $attributes = explode(',', $attribconfig);
                }
                if ($attributes) {
                    foreach ($attributes as $attribute) {
            ?>
                <fieldset>
                <legend><?php echo $attribute; ?></legend>
                <table>
                    <tbody>
                        <tr>
                            <td><?php echo $elements[$attribute.'_useattrib']['labelhtml']; ?></td>
                            <td><?php echo $elements[$attribute.'_useattrib']['html']; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $elements[$attribute.'_filterwords']['labelhtml']; ?></td>
                            <td><?php echo $elements[$attribute.'_filterwords']['html']; ?></td>
                        </tr>
                    </tbody>
                </table>
                </fieldset>
            <?php
                    }
                }
                else {
                    echo "<p>" . get_string('nofilteringattributes', 'artefact.campusconnect') . "</p>";
                }
            ?>
            </td>
        </tr>
        <tr class="submit">
            <td><?php echo $elements['submit']['html']; ?></td>
        </tr>
    </tbody>
</table>
</fieldset>

<?php
echo $hidden_elements;
echo '</form>';
?>
