{include file="header.tpl"}

<h2>{str tag=manageparticipants section=artefact.campusconnect}</h2>
<div id="allparts rel">
{if $html == ''}
    <div>{str tag=noenabledonlinehosts section=artefact.campusconnect}</div>
{else}
    <div>{$html|clean_html|safe}</div>
{/if}
</div>

{include file="footer.tpl"}
