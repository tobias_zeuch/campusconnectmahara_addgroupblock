<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'links');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');
require_once('pieforms/pieform.php');

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


$id = param_integer('id');

$data = get_record('artefact_campusconnect_courselink', 'id', $id);
$link = new ArtefactTypeCourselink($data);
$title = $link->get_title();
$metadata = $link->get_metadata();

$ecsid = $link->get_ecs_id();
$mid = $link->get_mid();
$resid = $link->get_resource_id();
$part = new ArtefactTypeParticipant($ecsid, $mid);
$imported = $part->get_displayname() . ' (' . $ecsid . '_' . $mid . ')';

$data = array();
$attributes = get_records_menu('artefact_campusconnect_attribute', '', '', 'attrib', 'attrib,term');
foreach ($attributes as $attrib => $term) {
    $value = (isset($metadata->$attrib) ? $metadata->$attrib : '');
    $data[$attrib] = array(
        'label' => (!empty($term) ? $term : $attrib),
        'value' => $value,
    );
}


$smarty = smarty();
$smarty->assign('title', $title);
$smarty->assign('imported', $imported);
$smarty->assign('resid', $resid);
$smarty->assign('data', $data);
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->display('artefact:campusconnect:metadata.tpl');
