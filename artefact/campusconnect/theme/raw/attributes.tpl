{include file="header.tpl"}

<h2>{str tag="courselinkfiltering" section="artefact.campusconnect"}&nbsp;>&nbsp;{str tag="attribmapping" section="artefact.campusconnect"}</h2>
<div class="rbuttons"><a href="filtering.php" class="btn">{str tag="backtofiltering" section="artefact.campusconnect"}</a></div>

{$form|safe}

{include file="footer.tpl"}
