<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'index');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');
require_once('pieforms/pieform.php');

$id = param_integer('id', 0);

$data = array();
$url = array();
if ($id > 0) {
    $artefact = new ArtefactTypeEcs($id);
    $data = $artefact->get_values();

    $ecsenabled = $artefact->is_enabled();
    // If ECS server is disabled
    if ($ecsenabled == 0) {
        $data['enabled'] = 0;
    }
    // else ECS server is enabled (online/offline)
    else {
        $data['enabled'] = 1;
    }
    // Parse ECS host url...
    $ecshosturl = $artefact->get_url();
    if (isset($ecshosturl) && !empty($ecshosturl)) {
        $url = parse_url($ecshosturl);
    }
    else {
        $url = array();
    }
}


$form = pieform(array(
    'name' => 'campusconnect_ecs',
    'plugintype' => 'artefact',
    'pluginname' => 'campusconnect',
    'configdirs' => array(get_config('libroot') . 'form/', get_config('docroot') . 'artefact/campusconnect/form/'),
    'elements' => array(
        'connectionsettings' => array(
            'type'        => 'fieldset',
            'legend'      => get_string('connectionsettings', 'artefact.campusconnect'),
            'collapsible' => true,
            'collapsed'   => false,
            'elements'    => array(
                'enabled' => array(
                    'type'         => 'select',
                    'title'        => get_string('enabled', 'artefact.campusconnect'),
                    'options'      => array(
                        0 => get_string('enabled.no', 'artefact.campusconnect'),
                        1 => get_string('enabled.yes', 'artefact.campusconnect'),
                    ),
                    'defaultvalue' => (isset($data['enabled']) ? $data['enabled'] : 1),
                ),
                'name' => array(
                    'type'         => 'text',
                    'title'        => get_string('name', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['name']) ? $data['name'] : null),
                    'rules' => array('required' => true),
                ),
                'url' => array(
                    'type'         => 'text',
                    'title'        => get_string('url', 'artefact.campusconnect'),
                    'description'  => get_string('urldesc', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($url['host']) && isset($url['path']) ? $url['host'].$url['path'] : null),
                    'size' => 50,
                    'rules' => array('required' => true),
                ),
                'protocol' => array(
                    'type'         => 'select',
                    'title'        => get_string('protocol', 'artefact.campusconnect'),
                    'options'      => array(
                        'http' => get_string('http', 'artefact.campusconnect'),
                        'https' => get_string('https', 'artefact.campusconnect'),
                    ),
                    'defaultvalue' => (isset($url['scheme']) ? $url['scheme'] : 'http'),
                    'rules' => array('required' => true),
                ),
                'port' => array(
                    'type'         => 'text',
                    'title'        => get_string('port', 'artefact.campusconnect'),
                    'size'         => 4,
                    'defaultvalue' => (isset($url['port']) ? $url['port'] : null),
                ),
                'auth' => array(
                    'type'         => 'select',
                    'title'        => get_string('auth', 'artefact.campusconnect'),
                    'options'      => array(
                        1 => get_string('auth.none', 'artefact.campusconnect'),
                        2 => get_string('auth.pass', 'artefact.campusconnect'),
                        3 => get_string('auth.cert', 'artefact.campusconnect'),
                    ),
                    'defaultvalue' => (isset($data['auth']) ? $data['auth'] : 2),
                    'rules' => array('required' => true),
                ),
                'certpath' => array(
                    'type'         => 'text',
                    'title'        => get_string('certpath', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['certpath']) ? $data['certpath'] : null),
                    'disabled' => (!isset($data['auth']) || $data['auth'] != 3 ? true : false),
                    'size' => 70,
                ),
                'keypath' => array(
                    'type'         => 'text',
                    'title'        => get_string('keypath', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['keypath']) ? $data['keypath'] : null),
                    'disabled' => (!isset($data['auth']) || $data['auth'] != 3 ? true : false),
                    'size' => 70,
                ),
                'keypass' => array(
                    'type'         => 'password',
                    'title'        => get_string('keypass', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['keypass']) ? $data['keypass'] : null),
                    'disabled' => (!isset($data['auth']) || $data['auth'] != 3 ? true : false),
                ),
                'cacertpath' => array(
                    'type'         => 'text',
                    'title'        => get_string('cacertpath', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['cacertpath']) ? $data['cacertpath'] : null),
                    'disabled' => (!isset($data['auth']) || $data['auth'] != 3 ? true : false),
                    'size' => 70,
                ),
                'httpuser' => array(
                    'type'         => 'text',
                    'title'        => get_string('httpuser', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['httpuser']) ? $data['httpuser'] : null),
                    'disabled' => (isset($data['auth']) && $data['auth'] != 2 ? true : false),
                ),
                'httppass' => array(
                    'type'         => 'text',
                    'title'        => get_string('httppass', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['httppass']) ? $data['httppass'] : null),
                    'disabled' => (isset($data['auth']) && $data['auth'] != 2 ? true : false),
                ),
                'ecsauth' => array(
                    'type'         => 'text',
                    'title'        => get_string('ecsauth', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['ecsauth']) ? $data['ecsauth'] : null),
                    'disabled' => (!isset($data['auth']) || $data['auth'] != 1 ? true : false),
                ),
            ),
        ),
        'localsettings' => array(
            'type'        => 'fieldset',
            'legend'      => get_string('localsettings', 'artefact.campusconnect'),
            'collapsible' => true,
            'collapsed'   => false,
            'elements'    => array(
                'crontime' => array(
                    'type'         => 'crontime',
                    'title'        => get_string('crontime', 'artefact.campusconnect'),
                    'description'  => get_string('crontimedesc', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['crontime']) ? $data['crontime'] : 300),
                ),
                'notifycontent' => array(
                    'type'         => 'userlist',
                    'title'        => get_string('notifycontent', 'artefact.campusconnect'),
                    'description'  => get_string('notifycontentdesc', 'artefact.campusconnect'),
                    'defaultvalue' => (isset($data['notifycontent']) ? $data['notifycontent'] : null),
                    'filter' => false,
                    'lefttitle' => get_string('notifycontent_left', 'artefact.campusconnect'),
                    'righttitle' => get_string('notifycontent_right', 'artefact.campusconnect'),
                ),
            ),
        ),
        'id' => array(
            'type' => 'hidden',
            'value' => $id,
        ),
        'submit' => array(
            'type' => 'submitcancel',
            'value' => array(get_string('save', 'mahara'), get_string('cancel', 'mahara')),
            'goto' => get_config('wwwroot') . 'artefact/campusconnect/index.php',
        ),
    )
));

$js = <<<EOJS
jQuery(document).ready(function() {
    jQuery('#campusconnect_ecs_auth').change(function() {
        var auth = jQuery('#campusconnect_ecs_auth :selected').val();
        // Option "None (dev only)" selected...
        if (auth == 1) {
            jQuery('#campusconnect_ecs_certpath').attr('disabled', true);
            jQuery('#campusconnect_ecs_keypath').attr('disabled', true);
            jQuery('#campusconnect_ecs_keypass').attr('disabled', true);
            jQuery('#campusconnect_ecs_cacertpath').attr('disabled', true);
            jQuery('#campusconnect_ecs_httpuser').attr('disabled', true);
            jQuery('#campusconnect_ecs_httppass').attr('disabled', true);
            jQuery('#campusconnect_ecs_ecsauth').attr('disabled', false);
        }
        // Option "Username/Password" selected...
        if (auth == 2) {
            jQuery('#campusconnect_ecs_certpath').attr('disabled', true);
            jQuery('#campusconnect_ecs_keypath').attr('disabled', true);
            jQuery('#campusconnect_ecs_keypass').attr('disabled', true);
            jQuery('#campusconnect_ecs_cacertpath').attr('disabled', true);
            jQuery('#campusconnect_ecs_httpuser').attr('disabled', false);
            jQuery('#campusconnect_ecs_httppass').attr('disabled', false);
            jQuery('#campusconnect_ecs_ecsauth').attr('disabled', true);
        }
        // Option "Certificate/base" selected...
        if (auth == 3) {
            jQuery('#campusconnect_ecs_certpath').attr('disabled', false);
            jQuery('#campusconnect_ecs_keypath').attr('disabled', false);
            jQuery('#campusconnect_ecs_keypass').attr('disabled', false);
            jQuery('#campusconnect_ecs_cacertpath').attr('disabled', false);
            jQuery('#campusconnect_ecs_httpuser').attr('disabled', true);
            jQuery('#campusconnect_ecs_httppass').attr('disabled', true);
            jQuery('#campusconnect_ecs_ecsauth').attr('disabled', true);
        }
    });
});
EOJS;


$subtitle = get_string('managehosts', 'artefact.campusconnect') . ' > ';
if ($id == 0) {
    $subtitle .= get_string('addhost', 'artefact.campusconnect');
}
else {
    $subtitle .= get_string('edithost', 'artefact.campusconnect');
}

$smarty = smarty();
$smarty->assign('subtitle', $subtitle);
$smarty->assign('form', $form);
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->assign('INLINEJAVASCRIPT', $js);
$smarty->display('artefact:campusconnect:edithost.tpl');


function campusconnect_ecs_submit(Pieform $form, $values) {
    global $USER, $SESSION;

    // Compose ECS url from relevant parts, validate it
    // and overwrite previous 'url' value.
    $protocol = $values['protocol'];
    $port = $values['port'];
    // Strip http:// or https:// from the beginning and
    // trim slashes from the beginning and from the end...
    $domain = str_replace(array('http://', 'https://'),
                          array('', ''), $values['url']);
    $domain = trim($domain);      // trim whitespace
    $domain = trim($domain, '/'); // trim slashes

    if ($port) {
        $values['url'] = $protocol . '://' . $domain . ':' . $port;
    }
    else {
        $values['url'] = $protocol . '://' . $domain;
    }

    $ecs = new ArtefactTypeEcs($values['id']);
    $ecs->save_settings($values);

    $SESSION->add_ok_msg(get_string('ecssavedsuccessfully', 'artefact.campusconnect'));
    redirect(get_config('wwwroot') . 'artefact/campusconnect/index.php');
}
