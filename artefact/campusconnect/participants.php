<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'participants');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


$action = param_variable('action', null);

if ($action) {
    list($ecsid, $mid, $data, $type) = explode('|', base64_decode($action));
    $participant = new ArtefactTypeParticipant($ecsid, $mid);
    $settings = new StdClass();
    $settings->ecsid = $ecsid;
    $settings->mid   = $mid;

    // Toggle state for importing course links
    if ($type == 'links') {
        if ($data == 0) {
            $settings->import = 1;
        } else {
            $settings->import = 0;
        }
        $participant->save_settings($settings);
    }

    // Toggle state for generating ECS token
    if ($type == 'token') {
        if ($data == 0) {
            $settings->ecstoken = 1;
        } else {
            $settings->ecstoken = 0;
        }
        $participant->save_settings($settings);
    }

    // Refresh all content from participant
    if ($type == 'refresh') {
        ArtefactTypeCourselink::refresh_from_participant($ecsid, $mid);
    }    
}


$hosts = ArtefactTypeEcs::list_ecs();
$html = '';
if ($hosts) {
    foreach ($hosts as $host) {
        $ecs = new ArtefactTypeEcs($host->id);
        if ($ecs->is_online()) {
			$communities = ArtefactTypeParticipant::load_communities($ecs);
			foreach ($communities as $community) {
                $participants = array();
                foreach ($community->participants as $p) {
                    $actionstr  = $p->get_ecs_id() . '|' . $p->get_mid() . '|';
                    $participants[] = array_merge(
                        $p->get_values(),
                        array(
                            'linksaction' => base64_encode($actionstr.$p->is_import_enabled().'|links'),
                            'tokenaction' => base64_encode($actionstr.$p->is_token_enabled().'|token'),
                        )
                    );
                }
                $refresh = base64_encode($community->ecsid.'|0|0|refresh');
                $smarty = smarty();
                $smarty->assign('title', $community->name);
                $smarty->assign('refresh', $refresh);
                $smarty->assign('participants', $participants);
                $html .= $smarty->fetch('artefact:campusconnect:participant.tpl');
            }
        }
    }
}

$smarty = smarty();
$smarty->assign_by_ref('html', $html);
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->display('artefact:campusconnect:participants.tpl');
