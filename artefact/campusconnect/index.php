<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'index');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


if ($delete = param_integer('delete', 0)) {
    ArtefactTypeEcs::delete_form($delete);
}

$hosts = (object) array(
    'offset' => param_integer('offset', 0),
    'limit'  => param_integer('limit', 10),
);

list($hosts->count, $hosts->data) = ArtefactTypeEcs::get_ecs_list($hosts->limit, $hosts->offset);
ArtefactTypeEcs::build_ecs_list_html($hosts);


$smarty = smarty(array('paginator'));
$smarty->assign_by_ref('hosts', $hosts);
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->assign('INLINEJAVASCRIPT', 'addLoadEvent(function() {' . $hosts->pagination_js . '});');
$smarty->display('artefact:campusconnect:index.tpl');


function delete_ecs_submit(Pieform $form, $values) {
    global $SESSION;
    $ecs = new ArtefactTypeEcs($values['delete']);
    $ecs->delete();
    $SESSION->add_ok_msg(get_string('ecsdeleted', 'artefact.campusconnect'));
    redirect('/artefact/campusconnect/index.php');
}
