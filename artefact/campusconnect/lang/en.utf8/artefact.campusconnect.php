<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'CampusConnect';
$string['missingextensions'] = 'Sorry, you cannot use CampusConnect plugin because your PHP installation doesn\'t have one or more required extensions installed and/or enabled:';

// Tabs
$string['about'] = 'About';
$string['ecshosts'] = 'ECS hosts';
$string['participants'] = 'Participants';
$string['importedcourselinks'] = 'Imported course links';
$string['courselinkfiltering'] = 'Course link filtering';

// About screen
$string['aboutplugin'] = 'Plugin information';
$string['plugindesc'] = 'CampusConnect is integration of different elearning systems based on %sECSA%s. An ECSA is a service architecture for elearning based webservices. It provides mechanisms for communication and authorization between elearning systems among each other and management systems.';
$string['plugintitle'] = 'CampusConnect for Mahara';
$string['pluginversion'] = 'Plugin version %s';
$string['plugincopyright'] = 'Copyright 2014 onwards %sSynergy Learning%s';
$string['extensions'] = 'Required extensions';
$string['curlversion'] = 'cURL/%s extension installed and enabled';
$string['opensslversion'] = '%s extension installed and enabled';

// Manage ECS hosts
$string['host'] = 'ECS host';
$string['hosts'] = 'ECS hosts';
$string['active'] = 'Active';
$string['delete'] = 'Delete';
$string['deleteecs?'] = 'Are you sure you want to delete this ECS host?';
$string['ecsdeleted'] = 'ECS host deleted.';
$string['ecssavedsuccessfully'] = 'ECS host saved successfully.';

$string['managehosts'] = 'Manage ECS hosts';
$string['availablehosts'] = 'Available ECS hosts';
$string['addhost'] = 'Add new ECS host';
$string['edithost'] = 'Edit ECS host';
$string['serveraddress'] = 'Server address:';
$string['expirydate'] = 'Certificate expiry date:';
$string['youhavenohosts'] = 'No available ECS hosts.';
$string['noenabledonlinehosts'] = 'No enabled and online ECS hosts.';

$string['connectionsettings'] = 'Connection settings';
$string['enabled'] = 'ECS enabled';
$string['enabled.no'] = 'No';
$string['enabled.yes'] = 'Yes';
$string['enabled.online'] = '(Online)';
$string['enabled.offline'] = '(Offline)';
$string['name'] = 'Name';
$string['url'] = 'URL';
$string['urldesc'] = 'You must not include the http:// or https:// part';
$string['protocol'] = 'Protocol';
$string['http'] = 'HTTP';
$string['https'] = 'HTTPS';
$string['port'] = 'Port';
$string['auth'] = 'Authentication type';
$string['auth.none'] = 'None (dev only)';
$string['auth.cert'] = 'Certificate/base';
$string['auth.pass'] = 'Username/Password';
$string['certpath'] = 'Client certificate';
$string['keypath'] = 'Certificate key';
$string['keypass'] = 'Key password';
$string['cacertpath'] = 'CA certificate';
$string['httpuser'] = 'Username';
$string['httppass'] = 'Password';
$string['ecsauth'] = 'ECS auth id (dev only)';

$string['localsettings'] = 'Local settings';
$string['crontime'] = 'Minumum polling time';
$string['crontimedesc'] = 'Used to indicate how often ECS hosts should be polled using the CRON.';
$string['minutes'] = 'minutes';
$string['seconds'] = 'seconds';
$string['notifycontent'] = 'Notify about new content';
$string['notifycontent_left'] = 'Available users';
$string['notifycontent_right'] = 'Users to be notified';
$string['notifycontentdesc'] = 'List of users to send a notifications to when new content (course links) are imported.';

$string['notifylink_body'] = 'The following course links have been imported into %s:';
$string['notifylink_subject'] = '%s - newly imported course links';
$string['notifylink_update_body'] = 'The following imported course links have been updated in %s:';
$string['notifylink_update_subject'] = '%s - updated imported course links';
$string['notifylink_delete_body'] = 'The following course links are no longer imported into %s:';
$string['notifylink_delete_subject'] = '%s - removed imported course links';
$string['notifylink_error_body'] = 'There was an error creating course links for %s:';
$string['notifylink_error_subject'] = '%s - error importing course links';
$string['notifyuser_body'] = 'The following users have been created on %s:';
$string['notifyuser_subject'] = '%s - newly created users';

// Manage participants
$string['manageparticipants'] = 'Manage participants';
$string['refreshallcontent'] = 'Refresh all content';
$string['participants'] = 'Participants';
$string['furtherinformation'] = 'Further information';
$string['courselinkslabel'] = 'Links';
$string['ecstokenlabel'] = 'Token';
$string['provider'] = 'Provider';
$string['domainname'] = 'Domain name';
$string['abbreviation'] = 'Abbreviation';
$string['identifier'] = 'Participant ID';
$string['thisvle'] = 'This VLE';

// Data mapping
$string['datamapping'] = 'Data mapping';
$string['defaultdatamapping'] = 'Default user data mapping';
$string['userdatamapping'] = 'User data mapping';
$string['mappingsavedsuccessfully'] = 'Data mappings saved successfully.';

$string['mapping.not_mapped'] = 'Not mapped';
$string['mapping.ecs_email'] = 'Email';
$string['mapping.ecs_eppn'] = 'EPPN';
$string['mapping.ecs_firstname'] = 'Firstname';
$string['mapping.ecs_institution'] = 'Institution';
$string['mapping.ecs_lastname'] = 'Lastname';
$string['mapping.ecs_login'] = 'Login';
$string['mapping.ecs_loginid'] = 'Login ID';

// Imported course links
$string['youhavenolinks'] = 'No course links imported.';
$string['courselink'] = 'course link';
$string['courselinks'] = 'course links';
$string['title'] = 'Title';
$string['importedfrom'] = 'Imported from';
$string['resourceid'] = 'Resource ID';
$string['metadata'] = 'Meta data';
$string['organisation'] = 'Organisation';
$string['language'] = 'Language';
$string['term'] = 'Term';
$string['credits'] = 'Credits';
$string['status'] = 'Status';
$string['coursetype'] = 'Course type';
$string['moredetails'] = 'More details';
$string['backtolinks'] = 'Back to Imported course links';
$string['linkmetadata'] = 'Course link meta data';

$string['cannotbeempty'] = 'The field "%s" cannot be empty';
$string['remotefieldnotfound'] = 'The remote field "%s" does not exist';

// Course link filtering
$string['unknown'] = 'Unknown (deleted)';

$string['filteringenabled'] = 'Course links displayed controlled by';
$string['filteringenabledadmin'] = 'Configuration made at the admin level';
$string['filteringenabledgroup'] = 'Configuration made at the group admin level';
$string['attribute'] = 'Attribute';
$string['attributevalue'] = 'Attribute value';
$string['courseattributes'] = 'Course attributes';
$string['manageattributes'] = 'Manage attributes';
$string['noneselected'] = 'None selected';

$string['filteringsettings'] = 'Filtering settings';
$string['filtersettings'] = 'Filter settings';
$string['localgroups'] = 'Local groups';
$string['generalsettingssaved'] = 'General filtering settings saved successfully';
$string['groupsettingssaved'] = 'Group filtering settings saved successfully';

$string['nogroups'] = 'There are no groups to apply filtering settings. You can %screate one%s.';
$string['nofilteringattributes'] = 'Please set some course attributes to be used for course link filtering.';
$string['savesettings'] = 'Save settings';
$string['savechanges'] = 'Save changes';
$string['useattrib'] = 'Use attribute';
$string['allwords'] = 'All words';
$string['filterwords'] = 'Filter words';

$string['attribmapping'] = 'Attribute mapping';
$string['backtofiltering'] = 'Back to Filtering settings';
$string['attribnotused'] = 'Attribute not used anymore.';
$string['attribmappingsavedsuccessfully'] = 'Attribute mappings saved successfully.';

// Sideblock stuff
$string['sideblocktitle'] = 'Course links';
$string['nocourselinks'] = 'No course links for this group.';
$string['enrolment'] = 'Enrolment';
$string['viewcourse'] = 'View course';

// Member (enrolment) statuses
$string['active'] = 'active';
$string['pending'] = 'pending';
$string['rejected'] = 'rejected';
$string['unsubscribed'] = 'unsubscribed';
$string['inactive_account'] = 'inactive';
$string['not_enroled'] = 'not enroled';

// Logs
$string['viewlog'] = 'View logs';
