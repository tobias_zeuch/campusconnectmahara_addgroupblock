{include file="header.tpl"}

<h2>{str tag=importedcourselinks section=artefact.campusconnect}</h2>
<div id="alllinks rel">
{if !$links->data}
    <div>{str tag=youhavenolinks section=artefact.campusconnect}</div>
{else}
    <div id="linklist" class="fullwidth listing">
        {$links->tablerows|safe}
    </div>
    {$links->pagination|safe}
{/if}
</div>

{include file="footer.tpl"}
