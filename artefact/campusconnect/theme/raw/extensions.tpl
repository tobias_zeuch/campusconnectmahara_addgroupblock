{include file='header.tpl'}

<p>{str section=artefact.campusconnect tag=missingextensions}</p>
<ul>
{foreach from=$missingextensions item=extension}
    <li><a href="http://www.php.net/{$extension}">{$extension}</a></li>
{/foreach}
</ul>

{include file='footer.tpl'}