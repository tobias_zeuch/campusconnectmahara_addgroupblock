{include file="header.tpl"}

<h2>{$subtitle}</h2>
<p>{$plugindesc|safe}</p>

<h4>{$plugintitle}</h4>
<p>{$pluginversion}<br />{$plugincopyright|safe}</p>

<p><b>{$extensions}</b>
<ul>
    <li>{$curlversion}</li>
    <li>{$opensslversion}</li>
</ul>
</p>

<p><br /><a href="http://www.synergy-learning.com/" target="_blank">
<img src="{$WWWROOT}/artefact/campusconnect/theme/raw/static/images/synergylearning.png" border="0"></a></p>

{include file="footer.tpl"}
