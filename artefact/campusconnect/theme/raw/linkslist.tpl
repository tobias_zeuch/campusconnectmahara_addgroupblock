<table id="linkslist" class="fullwidth listing">
<thead>
    <tr>
        <th width="30%">{str tag=title section=artefact.campusconnect}</th>
        <th width="40%">{str tag=importedfrom section=artefact.campusconnect}</th>
        <th width="25%">{str tag=metadata section=artefact.campusconnect}</th>
        <th width="5%"></th>
    </tr>
</thead>
</tbody>
{foreach from=$links->data item=link}
    <tr class="{cycle name=rows values='r0,r1'} listrow">
        <td>
            <h3 class="title">{$link->title}</h3>
        </td>
        <td>
            {$link->importedfrom}
        </td>
        <td>
            {str tag=organisation section=artefact.campusconnect}: {if $link->metadata->organisation}{$link->metadata->organisation}{/if}<br />
            {str tag=language section=artefact.campusconnect}: {if $link->metadata->lang}{$link->metadata->lang}{/if}<br />
            {str tag=term section=artefact.campusconnect}: {if $link->metadata->term}{$link->metadata->term}{/if}<br />
            {str tag=credits section=artefact.campusconnect}: {if $link->metadata->credits}{$link->metadata->credits}{/if}<br />
            {str tag=status section=artefact.campusconnect}: {if $link->metadata->status}{$link->metadata->status}{/if}<br />
            {str tag=coursetype section=artefact.campusconnect}: {if $link->metadata->courseType}{$link->metadata->courseType}{/if}<br />
        </td>
        <td class="narrow center">
                <a href="{$WWWROOT}artefact/campusconnect/metadata.php?id={$link->id}" title="{str tag=metadata section=artefact.campusconnect}"><img src="{theme_url filename='images/btn_info.png'}" alt="{str tag=metadata section=artefact.campusconnect}"></a>
        </td>
    </tr>
{/foreach}
</tbody>
</table>
