<h3>{$title}</h3>
<div class="rbuttons">
    <a class="btn" href="{$WWWROOT}artefact/campusconnect/participants.php?action={$refresh}">{str tag=refreshallcontent section=artefact.campusconnect}</a>
</div>
<table id="partlist" class="fullwidth listing">
<thead>
    <tr>
        <th width="30%">{str tag=participants section=artefact.campusconnect}</th>
        <th width="55%">{str tag=furtherinformation section=artefact.campusconnect}</th>
        <th width="5%" class="center">{str tag=courselinkslabel section=artefact.campusconnect}</th>
        <th width="5%" class="center">{str tag=ecstokenlabel section=artefact.campusconnect}</th>
        <th width="5%"></th>
    </tr>
</thead>
</tbody>
{foreach from=$participants item=p}
    <tr class="{cycle name=rows values='r0,r1'} listrow">
        <td><h3 class="title">{$p.displayname}</h3>{if $p.itsyou}({str tag=thisvle section=artefact.campusconnect}){/if}</td>
        <td>
            {str tag=provider section=artefact.campusconnect}: {$p.org}<br />
            {str tag=domainname section=artefact.campusconnect}: {$p.dns}<br />
            {str tag=mapping.ecs_email section=artefact.campusconnect}: <a href="mailto:{$p.email}" title="{$p.email}">{$p.email}</a><br />
            {str tag=abbreviation section=artefact.campusconnect}: {$p.orgabbr}<br />
            {str tag=identifier section=artefact.campusconnect}: {$p.identifier}<br />
        </td>
        <td class="narrow center">
                <a href="{$WWWROOT}artefact/campusconnect/participants.php?action={$p.linksaction}">
                <img src="{$WWWROOT}artefact/campusconnect/theme/raw/static/images/{if $p.import}1{else}0{/if}.png"></a>
        </td>
        <td class="narrow center">
                <a href="{$WWWROOT}artefact/campusconnect/participants.php?action={$p.tokenaction}">
                <img src="{$WWWROOT}artefact/campusconnect/theme/raw/static/images/{if $p.ecstoken}1{else}0{/if}.png"></a>
        </td>
        <td class="narrow center">
                <a href="{$WWWROOT}artefact/campusconnect/mapping.php?id={$p.ecsid}&mid={$p.mid}&type=2" title="{str tag=datamapping section=artefact.campusconnect}"><img src="{theme_url filename='images/btn_configure.png'}" alt="{str tag=datamapping section=artefact.campusconnect}"></a>
        </td>
    </tr>
{/foreach}
</tbody>
</table>
