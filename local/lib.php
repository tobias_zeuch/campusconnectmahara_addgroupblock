<?php
/**
 * Library file for miscellaneous local customisations.
 *
 * For simple customisation of a Mahara site, the core code will call some local_* functions
 * which may be defined in this file.
 *
 * Functions that will be called by core:
 *  - local_main_nav_update($menu):        modify the main navigation menu in the header
 *  - local_xmlrpc_services():              add custom xmlrpc functions
 *  - local_can_remove_viewtype($viewtype): stop users from deleting views of a particular type
 */

function local_sideblocks_update(&$SIDEBLOCKS) {
    if (defined('MENUITEM') && MENUITEM != '') {
        // Get $id or $group parameters, which hold group ID
        $groupid = param_integer('id', param_integer('group', 0));
        $grouppages = array(
            'groups/info',
            'groups/members',
            'groups/forums',
            'groups/views',
            'groups/collections',
            'groups/share',
            'groups/files',
        );
        if (in_array(MENUITEM, $grouppages)) {
            safe_require('artefact', 'campusconnect');
            $SIDEBLOCKS[] = array(
                'name'   => 'courselinks',
                'id'     => 'sb-courselinks',
                'weight' => -10,
                'data'   => ArtefactTypeCourselink::courselinks_sideblock($groupid)
            );
        }
    }
}