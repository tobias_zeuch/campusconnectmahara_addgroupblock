{include file="header.tpl"}

<h2>{str tag="linkmetadata" section="artefact.campusconnect"}&nbsp;>&nbsp;{$title}</h2>
<div class="rbuttons"><a href="courselinks.php" class="btn">{str tag="backtolinks" section="artefact.campusconnect"}</a></div>

<p><label>{str tag="importedfrom" section="artefact.campusconnect"}:</label> {$imported}</p>
<p><label>{str tag="resourceid" section="artefact.campusconnect"}:</label> {$resid}</p>

<h4>{str tag="metadata" section="artefact.campusconnect"}</h4>
<table class="fullwidth">
    <thead>
        <tr>
            <th>{str tag="attribute" section="artefact.campusconnect"}</th>
            <th>{str tag="attributevalue" section="artefact.campusconnect"}</th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$data item=item}
        <tr class="{cycle values='r0,r1'}">
            <th>{$item.label}</th>
            <td>{$item.value}</td>
        </tr>
    {/foreach}
    </tbody>
</table>

{include file="footer.tpl"}
