<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('JSON', 1);

require(dirname(dirname(dirname(__FILE__))) . '/init.php');
require_once(get_config('libroot') . 'pieforms/pieform.php');
safe_require('artefact', 'campusconnect');

$hosts = (object) array(
    'offset' => param_integer('offset', 0),
    'limit'  => param_integer('limit', 10),
);

list($hosts->count, $hosts->data) = ArtefactTypeEcs::get_ecs_list($hosts->limit, $hosts->offset);
ArtefactTypeEcs::build_ecs_list_html($hosts);

json_reply(false, array('data' => $hosts));
