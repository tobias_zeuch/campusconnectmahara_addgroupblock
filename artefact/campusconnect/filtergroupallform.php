<?php
echo $form_tag;
?>

<fieldset>
<legend><?php echo get_string('filteringsettings', 'artefact.campusconnect'); ?></legend>
<?php
    // Get data about all the groups (not deleted!) to display them...
    $groups = get_records_array('group', 'deleted', 0);
    if (!$groups) {
        echo get_string('nogroups', 'artefact.campusconnect',
                        '<a href="'.get_config('wwwroot').'group/edit.php">', '</a>');
    }
    else {
?>
<table class="fullwidth">
    <thead>
        <tr>
            <th width="30%"><?php echo get_string('localgroups', 'artefact.campusconnect'); ?></th>
            <th width="70%"><?php echo get_string('filtersettings', 'artefact.campusconnect'); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><ul>
            <?php
                foreach ($groups as $group) {
                    if ($group->id == $elements['groupid']['value']) {
                        echo '<li><b>' . $group->name . '&nbsp;&nbsp;&nbsp;===></b></li>';
                    } else {
                        echo '<li><b><a href="' . get_config('wwwroot')
                             . 'artefact/campusconnect/filtering.php?groupid=' . $group->id
                             . '">' . $group->name . '</a></b></li>';
                    }
                }
            ?>
            </ul></td>
            <td>
            <?php
                $attribconfig = get_config_plugin('artefact', 'campusconnect', 'filterattributes');
                $attributes = array();
                if (isset($attribconfig) && !empty($attribconfig)) {
                    $attributes = explode(',', $attribconfig);
                }
                if ($attributes) {
                    foreach ($attributes as $attribute) {
            ?>
                <fieldset>
                <legend><?php echo $attribute; ?></legend>
                <table>
                    <tbody>
                        <tr>
                            <td><?php echo $elements[$attribute.'_useattrib']['labelhtml']; ?></td>
                            <td><?php echo $elements[$attribute.'_useattrib']['html']; ?></td>
                        </tr>
                        <tr>
                            <td><?php echo $elements[$attribute.'_filterwords']['labelhtml']; ?></td>
                            <td><?php echo $elements[$attribute.'_filterwords']['html']; ?></td>
                        </tr>
                    </tbody>
                </table>
                </fieldset>
            <?php
                    }
                }
                else {
                    echo "<p>" . get_string('nofilteringattributes', 'artefact.campusconnect') . "</p>";
                }
            ?>
            </td>
        </tr>
        <tr class="submit">
            <td></td>
            <td><?php echo $elements['submit']['html']; ?></td>
        </tr>
    </tbody>
</table>
<?php } ?>
</fieldset>

<?php
echo $hidden_elements;
echo '</form>';
?>
