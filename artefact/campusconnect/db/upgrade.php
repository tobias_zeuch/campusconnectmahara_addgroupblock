<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

defined('INTERNAL') || die();

function xmldb_artefact_campusconnect_upgrade($oldversion=0) {

    if ($oldversion < 2014041204) {
        safe_require('artefact', 'campusconnect');
        // Refresh ECS supported attributes upon installation
        $conn = new ArtefactTypeCampusconnect();
        $conn->refresh_supported_attributes();
    }

    if ($oldversion < 20140712000) {
        // Add 'artefact_campusconnect_status' table which holds
        // the data about course enrolment / member statuses
        $table = new XMLDBTable('artefact_campusconnect_status');
        $table->addFieldInfo('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, XMLDB_SEQUENCE);
        $table->addFieldInfo('courseid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL);
        $table->addFieldInfo('url', XMLDB_TYPE_CHAR, 255, null, XMLDB_NOTNULL);
        $table->addFieldInfo('resourceid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL);
        $table->addFieldInfo('ecsid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL);
        $table->addFieldInfo('mid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL);
        $table->addFieldInfo('status', XMLDB_TYPE_CHAR, 16, null, null, null,
            XMLDB_ENUM, array('active', 'pending', 'rejected', 'unsubscribed', 'inactive_account'), 'pending');
        $table->addFieldInfo('userid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL);
        $table->addKeyInfo('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->addKeyInfo('ecsfk', XMLDB_KEY_FOREIGN, array('ecsid'), 'artefact_campusconnect_ecs', array('artefact'));
        $table->addKeyInfo('userfk', XMLDB_KEY_FOREIGN, array('userid'), 'usr', array('id'));
        create_table($table);
    }

    return true;
}
