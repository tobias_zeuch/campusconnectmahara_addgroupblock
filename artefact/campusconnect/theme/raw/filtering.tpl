{include file="header.tpl"}

<h2>{str tag=courselinkfiltering section=artefact.campusconnect}</h2>
<div class="rbuttons"><a href="attributes.php" class="btn">{str tag="manageattributes" section="artefact.campusconnect"}</a></div>

{if $settingsform}{$settingsform|safe}{/if}
{$filteringform|safe}

{include file="footer.tpl"}
