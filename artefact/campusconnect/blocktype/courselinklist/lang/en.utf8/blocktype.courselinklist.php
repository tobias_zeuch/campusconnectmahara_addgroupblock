<?php
/**
 *
 * @package    mahara
 * @subpackage campusconnect-courselinklist
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @author     Tobias Zeuch <tobias.zeuch@kit.edu>
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Course link list';
$string['description'] = 'A list of links to courses relevant for this group';

$string['defaulttitledescription'] = 'Courselinklist';
