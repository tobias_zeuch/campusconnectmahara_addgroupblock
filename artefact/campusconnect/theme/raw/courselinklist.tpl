    <div class="blockinstance-header"><h3>{str tag=sideblocktitle section=artefact.campusconnect}</h3></div>
    <div class="blockinstance-content">
    {if $data}
        {foreach from=$data.links item=item}
        <table class="cb fullwidth">
            <tbody>
                <tr class="expandable-head">
                    <th><a class="toggle expandable" href="#">{$item.title}</a></th>
                </tr>
                <tr class="expandable-body">
                    <td>
                    <div>
                    <label>{str tag=organisation section=artefact.campusconnect}:</label> {$item.organisation}<br />
                    <label>{str tag=language section=artefact.campusconnect}:</label> {$item.lang}<br />
                    <label>{str tag=term section=artefact.campusconnect}:</label> {$item.term}<br />
                    <label>{str tag=credits section=artefact.campusconnect}:</label> {$item.credits}<br />
                    <label>{str tag=status section=artefact.campusconnect}:</label> {$item.status}<br />
                    <label>{str tag=coursetype section=artefact.campusconnect}:</label> {$item.coursetype}<br />
                    <label>{str tag=enrolment section=artefact.campusconnect}:</label> {$item.enrolment}<br />
                    </div>
                    <div class="right" style="margin-top:5px"><a href="{$item.url}" class="btn" title="{str tag=viewcourse section=artefact.campusconnect}">{str tag=viewcourse section=artefact.campusconnect}</a></div>
                    </td>
                </tr>
            </tbody>
        </table>
        {/foreach}
    {else}
        {str tag=nocourselinks section=artefact.campusconnect}
    {/if}
    </div>
    <script>
    $j = jQuery;
    $j(document).ready(function() {
        $j("#blockinstance_{$blockinstanceid} .expandable-body").hide();
        $j("#blockinstance_{$blockinstanceid} .toggle").addClass('expandable');
        $j("#blockinstance_{$blockinstanceid} .expandable-head").click(function(event) {
            $j(this).find("th a.toggle.expandable").toggleClass('expanded');
            $j(this).next(".expandable-body").toggle();
        });
    });
    </script>