{include file="header.tpl"}

<h2>{str tag=managehosts section=artefact.campusconnect}</h2>
<div class="rbuttons"><a href="edithost.php" class="btn">{str tag="addhost" section="artefact.campusconnect"}</a></div>

<h3>{str tag=availablehosts section=artefact.campusconnect}</h3>
<div id="allecs rel">
{if !$hosts->data}
    <div>{str tag=youhavenohosts section=artefact.campusconnect}</div>
{else}
    <div id="hostlist" class="fullwidth listing">
        {$hosts->tablerows|safe}
    </div>
    {$hosts->pagination|safe}
{/if}
</div>

{include file="footer.tpl"}
