<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'filtering');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');
require_once('pieforms/pieform.php');

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


$conn = new ArtefactTypeCampusconnect();
$conn->refresh_supported_attributes();

$attributes = get_records_array('artefact_campusconnect_attribute', '', '', 'attrib');

$elements = array();
$notactive = get_string('attribnotused', 'artefact.campusconnect');
foreach ($attributes as $attribute) {
    $elements[$attribute->attrib] = array(
        'type' => 'text',
        'labelhtml' => $attribute->attrib,
        'defaultvalue' => $attribute->term,
        'description' => (!$attribute->active ? $notactive : null),
        'disabled' => (!$attribute->active ? true : false),
    );
}

$form = pieform(array(
    'name' => 'campusconnect_attributes',
    'plugintype' => 'artefact',
    'pluginname' => 'campusconnect',
    'elements' => array(
        'attribmappings' => array(
            'type'        => 'fieldset',
            'collapsible' => false,
            'collapsed'   => false,
            'legend'      => get_string('attribmapping', 'artefact.campusconnect'),
            'elements'    => $elements,
        ),
        'submit' => array(
            'type' => 'submitcancel',
            'value' => array(get_string('savechanges', 'artefact.campusconnect'), get_string('cancel')),
            'goto' => get_config('wwwroot') . 'artefact/campusconnect/filtering.php',
        ),
    ),
));


$smarty = smarty();
$smarty->assign('form', $form);
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->display('artefact:campusconnect:attributes.tpl');


function campusconnect_attributes_submit(Pieform $form, $values) {
    global $SESSION;
    // Unset unused values
    unset($values['sesskey']);
    unset($values['submit']);

    foreach ($values as $key => $value) {
        if (!is_null($value) && !empty($value)) {
            $where = new StdClass();
            $where->attrib = $key;
            $update = new StdClass();
            $update->term = (isset($value) ? $value : '');
            update_record('artefact_campusconnect_attribute', $update, $where);
        }
    }

    $SESSION->add_ok_msg(get_string('attribmappingsavedsuccessfully', 'artefact.campusconnect'));
    redirect(get_config('wwwroot') . 'artefact/campusconnect/attributes.php');
}
