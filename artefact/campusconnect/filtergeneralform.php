<?php
echo $form_tag;
?>

<div>
<?php echo $elements['filteringenabled']['labelhtml']; ?>&nbsp;
<?php echo $elements['filteringenabled']['html']; ?>
</div>

<fieldset>
<legend><?php echo get_string('courseattributes', 'artefact.campusconnect'); ?></legend>
<table>
    <tbody>
<?php for ($i = 0; $i < intval($elements['attribcount']['value']); $i++) { ?>
        <tr>
            <th><label for="campusconnect_filter_attribute_<?php echo $i;?>">
            <?php echo $elements['attribute_'.$i]['title']; ?></label></th>
            <td><?php echo $elements['attribute_'.$i]['html']; ?></td>
        </tr>
<?php } ?>
        <tr class="submit">
            <td></td>
            <td><?php echo $elements['submit']['html']; ?></td>
        </tr>
    </tbody>
</table>
</fieldset>

<?php
echo $hidden_elements;
echo '</form>';
?>
<br />
