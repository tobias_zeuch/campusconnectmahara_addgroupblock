    <script>
    $j = jQuery;
    $j(document).ready(function() {
        $j(".expandable-body").hide();
        $j(".toggle").addClass('expandable');
        $j(".expandable-head").click(function(event) {
            $j(this).find("th a.toggle.expandable").toggleClass('expanded');
            $j(this).next(".expandable-body").toggle();
        });
    });
    </script>
    <div class="sidebar-header"><h3>{str tag=sideblocktitle section=artefact.campusconnect}</h3></div>
    <div class="sidebar-content">
    {if $sbdata}
        {foreach from=$sbdata.links item=item}
        <table class="cb fullwidth">
            <tbody>
                <tr class="expandable-head">
                    <th><a class="toggle expandable" href="#">{$item.title}</a></th>
                </tr>
                <tr class="expandable-body">
                    <td>
                    <div>
                    <label>{str tag=organisation section=artefact.campusconnect}:</label> {$item.organisation}<br />
                    <label>{str tag=language section=artefact.campusconnect}:</label> {$item.lang}<br />
                    <label>{str tag=term section=artefact.campusconnect}:</label> {$item.term}<br />
                    <label>{str tag=credits section=artefact.campusconnect}:</label> {$item.credits}<br />
                    <label>{str tag=status section=artefact.campusconnect}:</label> {$item.status}<br />
                    <label>{str tag=coursetype section=artefact.campusconnect}:</label> {$item.coursetype}<br />
                    <label>{str tag=enrolment section=artefact.campusconnect}:</label> {$item.enrolment}<br />
                    </div>
                    <div class="right" style="margin-top:5px"><a href="{$item.url}" class="btn" title="{str tag=viewcourse section=artefact.campusconnect}">{str tag=viewcourse section=artefact.campusconnect}</a></div>
                    </td>
                </tr>
            </tbody>
        </table>
        {/foreach}
        {if $sbdata.access == 'admin'}
            <br /><div><a href="{$WWWROOT}artefact/campusconnect/filtering.php?groupid={$sbdata.groupid}" class="btn" title="{str tag=courselinkfiltering section=artefact.campusconnect}"><span class="btn-config">{str tag=courselinkfiltering section=artefact.campusconnect}</span></a></div>
        {/if}
    {else}
        {str tag=nocourselinks section=artefact.campusconnect}
    {/if}
    </div>
