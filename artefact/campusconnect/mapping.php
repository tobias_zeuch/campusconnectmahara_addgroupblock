<?php
/**
 *
 * @package    mahara
 * @subpackage artefact-campusconnect
 * @author     Gregor Anzelj <gregor.anzelj@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL version 3 or later
 * @copyright  For copyright information on Mahara, please see the README file distributed with this software.
 * @copyright  2014 onwards Synergy Learning
 * @link       http://www.synergy-learning.com/
 *
 */

define('INTERNAL', 1);
define('ADMIN', 1);
define('MENUITEM', 'configsite/campusconnect');
define('SECTION_PLUGINTYPE', 'artefact');
define('SECTION_PLUGINNAME', 'campusconnect');
define('SECTION_PAGE', 'index');
define('CAMPUSCONNECT_SUBPAGE', 'mapping');

require_once(dirname(dirname(dirname(__FILE__))) . '/init.php');
define('TITLE', get_string('pluginname', 'artefact.campusconnect'));
safe_require('artefact', 'campusconnect');
require_once('pieforms/pieform.php');

$ecsid = param_integer('id');
$mid = param_variable('mid', null);
$type = param_integer('type', ArtefactTypeMetadata::TYPE_ECS_SERVER);

// Check if required extensions are installed and enabled
$opensslext = extension_loaded('openssl');
$curlext    = extension_loaded('curl');
if (!$opensslext || !$curlext) {
    $smarty = smarty();
    $missingextensions = array();
    !$curlext    && $missingextensions[] = 'curl';
    !$opensslext && $missingextensions[] = 'openssl';
    $smarty->assign('missingextensions', $missingextensions);
    $smarty->display('artefact:campusconnect:extensions.tpl');
    exit;
}


/*
$type = get_field('artefact', 'artefacttype', 'id', $id);
if (!in_array($type, array('ecs', 'participant'))) {
    throw new FeatureNotEnabledException("Data mappings are not supported for $type artefacts.");
}
*/
switch ($type) {
    case ArtefactTypeMetadata::TYPE_ECS_SERVER:
        // ECS default data mapping
        $subtitle  = get_string('managehosts', 'artefact.campusconnect') . ' > ';
        $subtitle .= get_string('defaultdatamapping', 'artefact.campusconnect');
        $legend = get_string('defaultdatamapping', 'artefact.campusconnect');
        $goto = get_config('wwwroot') . 'artefact/campusconnect/index.php';
        $courselink = false;
        break;
    case ArtefactTypeMetadata::TYPE_PARTICIPANT:
        // Participant data mapping
        $subtitle  = get_string('manageparticipants', 'artefact.campusconnect') . ' > ';
        $subtitle .= get_string('userdatamapping', 'artefact.campusconnect');
        $legend = get_string('userdatamapping', 'artefact.campusconnect');
        $goto = get_config('wwwroot') . 'artefact/campusconnect/participants.php';
        $courselink = false;
        break;
    case ArtefactTypeMetadata::TYPE_COURSE_LINK:
        // TODO!
        $goto = get_config('wwwroot') . 'artefact/campusconnect/filtering.php';
        $courselink = true;
        break;
}

$ecs = new ArtefactTypeEcs($ecsid);
$metadata = new ArtefactTypeMetadata($ecs, $mid, $courselink);
$mappings = $metadata->metadatamappings;
$options  = $metadata->get_metadata_options($type);


$form = pieform(array(
    'name' => 'campusconnect_mapping',
    'plugintype' => 'artefact',
    'pluginname' => 'campusconnect',
    'configdirs' => array(get_config('libroot') . 'form/', get_config('docroot') . 'artefact/campusconnect/form/'),
    'elements' => array(
        'datamappings' => array(
            'type'        => 'fieldset',
            'collapsible' => false,
            'collapsed'   => false,
            'legend'      => $legend,
            'elements'    => array(
                'eppn' => array(
                    'type'         => 'select',
                    'title'        => get_string('mapping.ecs_eppn', 'artefact.campusconnect'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['eppn']) ? $mappings['eppn'] : '0'),
                ),
                'username' => array(
                    'type'         => 'select',
                    'title'        => get_string('username', 'mahara'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['username']) ? $mappings['username'] : '0'),
                ),
                'email' => array(
                    'type'         => 'select',
                    'title'        => get_string('mapping.ecs_email', 'artefact.campusconnect'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['email']) ? $mappings['email'] : '0'),
                ),
                'institution' => array(
                    'type'         => 'select',
                    'title'        => get_string('institution', 'artefact.internal'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['institution']) ? $mappings['institution'] : '0'),
                ),
                'firstname' => array(
                    'type'         => 'select',
                    'title'        => get_string('firstname', 'artefact.internal'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['firstname']) ? $mappings['firstname'] : '0'),
                ),
                'lastname' => array(
                    'type'         => 'select',
                    'title'        => get_string('lastname', 'artefact.internal'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['lastname']) ? $mappings['lastname'] : '0'),
                ),
                'preferredname' => array(
                    'type'         => 'select',
                    'title'        => get_string('preferredname', 'artefact.internal'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['preferredname']) ? $mappings['preferredname'] : '0'),
                ),
                'studentid' => array(
                    'type'         => 'select',
                    'title'        => get_string('studentid', 'artefact.internal'),
                    'options'      => $options,
                    'defaultvalue' => (isset($mappings['studentid']) ? $mappings['studentid'] : '0'),
                ),
            ),
        ),
           'ecsid' => array(
               'type' => 'hidden',
               'value' => $ecsid,
           ),
           'mid' => array(
               'type' => 'hidden',
               'value' => $mid,
           ),
           'type' => array(
               'type' => 'hidden',
               'value' => $type,
           ),
           'courselink' => array(
               'type' => 'hidden',
               'value' => $courselink,
           ),
        'submit' => array(
            'type' => 'submitcancel',
            'value' => array(get_string('save', 'mahara'), get_string('cancel', 'mahara')),
            'goto' => $goto,
        ),
    )
));


$smarty = smarty();
$smarty->assign('subtitle', $subtitle);
$smarty->assign('form', $form);
$smarty->assign('PAGEHEADING', TITLE);
$smarty->assign('SUBPAGENAV', PluginArtefactCampusconnect::submenu_items());
$smarty->display('artefact:campusconnect:edithost.tpl');


function campusconnect_mapping_submit(Pieform $form, $values) {
    global $SESSION;

    $ecsid = $values['ecsid'];
    $mid = $values['mid'];
    $type = $values['type'];
    $courselink = $values['courselink'];

    switch ($type) {
        case ArtefactTypeMetadata::TYPE_ECS_SERVER:
            $redirect = get_config('wwwroot') . 'artefact/campusconnect/index.php';
            break;
        case ArtefactTypeMetadata::TYPE_PARTICIPANT:
            $redirect = get_config('wwwroot') . 'artefact/campusconnect/participants.php';
            break;
        case ArtefactTypeMetadata::TYPE_COURSE_LINK:
            $redirect = get_config('wwwroot') . 'artefact/campusconnect/filtering.php';
            break;
    }

    $ecs = new ArtefactTypeEcs($ecsid);
    $metadata = new ArtefactTypeMetadata($ecs, $mid, $courselink);
    $validfields = array_keys($metadata->get_metadata_mappings());

    foreach ($values as $field => $value) {
        if (!in_array($field, $validfields)) {
            unset($values[$field]);
        }
    }
    $metadata->save_data_mappings($values, $mid, $type);
    $SESSION->add_ok_msg(get_string('mappingsavedsuccessfully', 'artefact.campusconnect'));
    redirect($redirect);
}
