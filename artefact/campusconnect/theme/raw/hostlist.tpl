<table id="hostlist" class="fullwidth listing">
<thead>
    <tr>
        <th class="btns2 center">{str tag=active section=artefact.campusconnect}</th>
        <th>{str tag=name section=artefact.campusconnect}</th>
        <th colspan="3"></th>
    </tr>
</thead>
</tbody>
{foreach from=$hosts->data item=host}
    <tr class="{cycle name=rows values='r0,r1'} listrow">
        <td class="center">
            {if $host->enabled == 0}{str tag=enabled.no section=artefact.campusconnect}{else}{str tag=enabled.yes section=artefact.campusconnect}<br />
                {if $host->enabled == 1}{str tag=enabled.offline section=artefact.campusconnect}{else}{str tag=enabled.online section=artefact.campusconnect}{/if}
            {/if}
        </td>
        <td>
            <h3 class="title">{$host->title}</h3>
            <p>{$host->ecsinfo|clean_html|safe}</p>
        </td>
        <td class="narrow">
                <a href="{$WWWROOT}artefact/campusconnect/mapping.php?id={$host->id}" title="{str tag=datamapping section=artefact.campusconnect}"><img src="{theme_url filename='images/btn_configure.png'}" alt="{str tag=datamapping section=artefact.campusconnect}"></a>
        </td>
        <td class="narrow">
                <a href="{$WWWROOT}artefact/campusconnect/edithost.php?id={$host->id}" title="{str tag=edit}"><img src="{theme_url filename='images/btn_edit.png'}" alt="{str tag=edit}"></a>
        </td>
        <td class="narrow">
                {$host->deleteform|safe}
        </td>
    </tr>
{/foreach}
</tbody>
</table>
